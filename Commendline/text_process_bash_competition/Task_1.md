# Problem 1
You are given two access logs from a web application (mars reader),  primary\_node.log  secondary\_node.log

## The Task
Complete the script ```top_requesting_ips.sh``` so that it produces the client IPs (of the client VM, not the load balancer IP; not all requests have a client IP) along with their request counts, descending, from the two logs combined, within the time range 18:30 (inclusive, GMT) and 18:31 (exclusive, GMT).
Your script will be executed on a linux machine, so make sure your flags work on the gnu variants.
You should output to stdout according to this format:

````
num_requests_1 IP_1
num_requests_2 IP_2
...
````

e.g.
````
150 192.168.0.1
142 192.168.0.2
...
````

**If multiple IP addresses have the same count, they should be sorted lexicographically, descending.**

## The Log Format
An example line is below:
````
10.11.128.65 - - [13/Jan/2019:19:01:01 +0000] "GET /mars-reader/product/marskey/fantasticGB/id/11708613/medium HTTP/1.1" 200 3458 4 FeedProcessor 10.11.161.70 gzip
````
````
<load balancer IP> - - [timestamp] "<HTTP verb> <request path> <HTTP protocol version>" <response code> <response size (b)> <time taken to respond (ms)> <source app (may contain spaces)> <client IP> <compression flag>
````

## Submitting your code
* Take a branch of this repo called firstname.lastname
* Push your branch to origin
* Watch the build job at <todo>
